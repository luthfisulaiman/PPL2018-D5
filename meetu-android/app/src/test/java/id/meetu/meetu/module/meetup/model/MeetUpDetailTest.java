package id.meetu.meetu.module.meetup.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by luthfi on 20/03/18.
 */


@RunWith(PowerMockRunner.class)
public class MeetUpDetailTest {

    @Test
    public void testConstructMeetUpDetail() {
        MeetUpDetail detail = new MeetUpDetail();
        assertNotNull(detail);
    }

    @Test
    public void testConstructMeetUpDetaiWithParaml() {
        int id = 0;
        String title = "Title";
        String groups = "Groups";
        String date = "Date";
        String locationTime = "Location Time";

        MeetUpDetail detail = new MeetUpDetail(id, title, groups, date, locationTime);
        assertNotNull(detail);
    }

    @Test
    public void testSetterGetterId() {
        MeetUpDetail detail = new MeetUpDetail();
        detail.setId(1);
        assertEquals(detail.getId(), 1);
    }

    @Test
    public void testSetterGetterTitle() {
        MeetUpDetail detail = new MeetUpDetail();
        detail.setTitle("Title1");
        assertEquals(detail.getTitle(), "Title1");
    }
    @Test
    public void testSetterGetterGroups() {
        MeetUpDetail detail = new MeetUpDetail();
        detail.setGroups("Group1");
        assertEquals(detail.getGroups(), "Group1");
    }

    @Test
    public void testSetterGetterDate() {
        MeetUpDetail detail = new MeetUpDetail();
        detail.setDate("Date1");
        assertEquals(detail.getDate(), "Date1");
    }

    @Test
    public void testSetterGetterLocationTime() {
        MeetUpDetail detail = new MeetUpDetail();
        detail.setLocationTime("LocationTime1");
        assertEquals(detail.getLocationTime(), "LocationTime1");
    }
}

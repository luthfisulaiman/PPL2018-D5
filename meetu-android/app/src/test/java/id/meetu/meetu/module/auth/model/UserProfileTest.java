package id.meetu.meetu.module.auth.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.module.group.model.Group;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by ricky on 12/03/18.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class UserProfileTest {

    @Test
    public void testConstructModel() {
        UserProfile userProfile = new UserProfile();
        assertNotNull(userProfile);
    }

    @Test
    public void testSetterGetterUserId() {
        UserProfile userProfile = new UserProfile();
        userProfile.setUserId(1);
        assertEquals(userProfile.getUserId(), 1);
    }

    @Test
    public void testSetterGetterPhoneNumber() {
        UserProfile userProfile = new UserProfile();
        userProfile.setPhoneNumber("08123123123");
        assertEquals(userProfile.getPhoneNumber(), "08123123123");
    }

    @Test
    public void testSetterGetterLocation() {
        UserProfile userProfile = new UserProfile();
        userProfile.setLocation("Jakarta Pusat");
        assertEquals(userProfile.getLocation(), "Jakarta Pusat");
    }

    @Test
    public void testSetterGetterGroups() {
        UserProfile userProfile = new UserProfile();
        List<Group> groups = new ArrayList<>();
        userProfile.setGroups(groups);
        assertEquals(userProfile.getGroups(), groups);
    }
}

package id.meetu.meetu.module.meetup.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.Date;

import id.meetu.meetu.BuildConfig;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by ricky on 11/03/18.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MeetUpTest {

    @Test
    public void testConstructModel() {
        MeetUp meetUp = new MeetUp();
        assertNotNull(meetUp);
    }

    @Test
    public void testSetterGetterId() {
        MeetUp meetUp = new MeetUp();
        meetUp.setId(1);
        assertEquals(meetUp.getId(), 1);
    }

    @Test
    public void testSetterGetterTitle() {
        MeetUp meetUp = new MeetUp();
        meetUp.setTitle("MeetUp Ricky");
        assertEquals(meetUp.getTitle(), "MeetUp Ricky");
    }

    @Test
    public void testSetterGetterGroupId() {
        MeetUp meetUp = new MeetUp();
        meetUp.setGroupId(1);
        assertEquals(meetUp.getGroupId(), 1);
    }

    @Test
    public void testSetterGetterStartTime() {
        MeetUp meetUp = new MeetUp();
        Date currDate = new Date();
        meetUp.setStartTime(currDate);
        assertEquals(meetUp.getStartTime(), currDate);
    }

    @Test
    public void testSetterGetterEndTime() {
        MeetUp meetUp = new MeetUp();
        Date currDate = new Date();
        meetUp.setEndTime(currDate);
        assertEquals(meetUp.getEndTime(), currDate);
    }

}

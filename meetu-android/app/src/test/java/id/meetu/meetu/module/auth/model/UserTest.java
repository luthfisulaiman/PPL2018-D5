package id.meetu.meetu.module.auth.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import id.meetu.meetu.BuildConfig;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by ricky on 11/03/18.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class UserTest {

    @Test
    public void testConstructModel() {
        User user = new User();
        assertNotNull(user);
    }

    @Test
    public void testSetterGetterId() {
        User user = new User();
        user.setId(1);
        assertEquals(user.getId(), 1);
    }

    @Test
    public void testSetterGetterUsername() {
        User user = new User();
        user.setUsername("rickyputra");
        assertEquals(user.getUsername(), "rickyputra");
    }

    @Test
    public void testSetterGetterUserProfile() {
        User user = new User();
        UserProfile userProfile = new UserProfile();
        user.setUserProfile(userProfile);
        assertNotNull(user.getUserProfile());
    }

}

package id.meetu.meetu.module.meetup.model;

import static org.junit.Assert.assertEquals;

import id.meetu.meetu.module.group.model.GroupResponse;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 27/03/18.
 */
@RunWith(PowerMockRunner.class)
public class MeetUpResponseTest {

  private MeetUpResponse meetUpResponse;

  @Before
  public void setup() {
    String title = "asd";
    GroupResponse groupResponse = new GroupResponse(1, "dsa");
    Date startTime = new Date(2018, 3, 1);
    Date endTime = new Date(2018, 3, 2);

    meetUpResponse = new MeetUpResponse(title, groupResponse, startTime, endTime);
  }

  @Test
  public void testConstructMeetUpResponse() {
    String title = "asd";
    GroupResponse groupResponse = new GroupResponse(1, "dsa");
    Date startTime = new Date(2018, 3, 1);
    Date endTime = new Date(2018, 3, 2);

    MeetUpResponse response = new MeetUpResponse(title, groupResponse, startTime, endTime);

    assertEquals(title, response.getTitle());
    assertEquals(groupResponse, response.getGroupResponse());
    assertEquals(startTime, response.getStartTime());
    assertEquals(endTime, response.getEndTime());
  }

  @Test
  public void testSetterGetterTitle() {
    String title = "lol";

    meetUpResponse.setTitle(title);
    assertEquals(title, meetUpResponse.getTitle());
  }

  @Test
  public void testSetterGetterGroupResponse() {
    GroupResponse groupResponse = new GroupResponse(3, "sda");

    meetUpResponse.setGroupResponse(groupResponse);
    assertEquals(groupResponse, meetUpResponse.getGroupResponse());
  }

  @Test
  public void testSetterGetterStartTime() {
    Date startTime = new Date(2018, 3, 3);

    meetUpResponse.setStartTime(startTime);
    assertEquals(startTime, meetUpResponse.getStartTime());
  }

  @Test
  public void testSetterGetterEndTime() {
    Date endTime = new Date(2018, 3, 3);

    meetUpResponse.setEndTime(endTime);
    assertEquals(endTime, meetUpResponse.getEndTime());
  }
}

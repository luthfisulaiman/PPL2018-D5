package id.meetu.meetu.module.schedule.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.Date;

import id.meetu.meetu.BuildConfig;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by ricky on 11/03/18.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class ScheduleTest {
    @Test
    public void testConstructModel() {
        Schedule schedule = new Schedule();
        assertNotNull(schedule);
    }

    @Test
    public void testSetterGetterId() {
        Schedule schedule = new Schedule();
        schedule.setId(1);
        assertEquals(schedule.getId(), 1);
    }

    @Test
    public void testSetterGetterTitle() {
        Schedule schedule = new Schedule();
        schedule.setTitle("Schedule Ricky");
        assertEquals(schedule.getTitle(), "Schedule Ricky");
    }

    @Test
    public void testSetterGetterGroupId() {
        Schedule schedule = new Schedule();
        schedule.setParentId("parent1");
        assertEquals(schedule.getParentId(), "parent1");
    }

    @Test
    public void testSetterGetterStartTime() {
        Schedule schedule = new Schedule();
        Date currDate = new Date();
        schedule.setStartTime(currDate);
        assertEquals(schedule.getStartTime(), currDate);
    }

    @Test
    public void testSetterGetterEndTime() {
        Schedule schedule = new Schedule();
        Date currDate = new Date();
        schedule.setEndTime(currDate);
        assertEquals(schedule.getEndTime(), currDate);
    }
}

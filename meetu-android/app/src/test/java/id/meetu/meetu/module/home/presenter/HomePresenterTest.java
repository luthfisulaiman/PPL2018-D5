package id.meetu.meetu.module.home.presenter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.group.model.GroupResponse;
import id.meetu.meetu.module.meetup.model.MeetUpDetail;
import id.meetu.meetu.module.meetup.model.MeetUpResponse;
import id.meetu.meetu.module.meetup.service.MeetUpService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 27/03/18.
 */

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class HomePresenterTest {

  @Mock
  private HomePresenterListener listenerMock;

  @Mock
  private MeetUpService meetUpServiceMock;

  private HomePresenter homePresenter;

  @Before
  public void setUp() {
    listenerMock = mock(HomePresenterListener.class);
    meetUpServiceMock = mock(MeetUpService.class);
    homePresenter = new HomePresenter(listenerMock, meetUpServiceMock);
  }

  @Test
  public void testConstructHomePresenter1() {
    homePresenter = new HomePresenter(listenerMock);

    assertTrue(homePresenter instanceof HomePresenter);
    assertEquals(listenerMock, homePresenter.getListener());
  }

  @Test
  public void testConstructHomePresenter2() {
    homePresenter = new HomePresenter(listenerMock, meetUpServiceMock);

    assertTrue(homePresenter instanceof HomePresenter);
    assertEquals(listenerMock, homePresenter.getListener());
    assertEquals(meetUpServiceMock, homePresenter.getMeetUpService());
  }

  @Test
  public void testGetMeetUpsSuccess() {
    final Call<List<MeetUpResponse>> callMeetUpMock = mock(Call.class);

    // Date month use 0-based indexing, hence, this one is March
    // and year is actually 1900 + year
    MeetUpResponse meetUpResponse = new MeetUpResponse("makan", new GroupResponse(1, "rickybau"),
        new Date(118, 2, 1, 0, 0), new Date(118, 2, 1, 1, 15));
    final List<MeetUpResponse> responseBody = new ArrayList<>();
    responseBody.add(meetUpResponse);

    MeetUpDetail meetUpDetail = new MeetUpDetail(1, "makan", "rickybau", "Thursday, 01 March",
        "00.00 - 01.15 @ MIC");

    final Response<List<MeetUpResponse>> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(responseBody);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<MeetUpResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMeetUpMock, responseMock);

        return null;
      }
    }).when(meetUpServiceMock).getMeetUps(any(Callback.class));

    homePresenter.getMeetUps();

    ArgumentCaptor<List> argument = ArgumentCaptor.forClass(List.class);
    verify(listenerMock, times(1)).onGetSuccess(argument.capture());

    MeetUpDetail resultDetail = (MeetUpDetail) argument.getValue().get(0);

    assertEquals(meetUpDetail.getId(), resultDetail.getId());
    assertEquals(meetUpDetail.getDate(), resultDetail.getDate());
    assertEquals(meetUpDetail.getGroups(), resultDetail.getGroups());
    assertEquals(meetUpDetail.getTitle(), resultDetail.getTitle());
    assertEquals(meetUpDetail.getLocationTime(), resultDetail.getLocationTime());
  }

  @Test
  public void testGetMeetUpsNotSuccess() {
    final Call<List<MeetUpResponse>> callMeetUpMock = mock(Call.class);
    final Response<List<MeetUpResponse>> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<MeetUpResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMeetUpMock, responseMock);

        return null;
      }
    }).when(meetUpServiceMock).getMeetUps(any(Callback.class));

    homePresenter.getMeetUps();

    verify(listenerMock, times(1)).onError("Something is wrong");
  }

  @Test
  public void testGetMeetUpsFailure() {
    final Call<List<MeetUpResponse>> callMeetUpMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<MeetUpResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callMeetUpMock, throwable);

        return null;
      }
    }).when(meetUpServiceMock).getMeetUps(any(Callback.class));

    homePresenter.getMeetUps();

    verify(listenerMock, times(1)).onError("Something is wrong with your connection");
  }
}

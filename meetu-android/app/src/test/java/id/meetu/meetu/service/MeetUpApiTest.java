package id.meetu.meetu.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.service.MeetUpApi.Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 27/03/18.
 */

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
public class MeetUpApiTest {

  @Test
  public void testFindUrl() {
    String url = new MeetUpApi().findUrl();

    assertEquals(BuildConfig.BACKEND_URL, url);
  }

  @Test
  public void testCreate() {
    Service service = new MeetUpApi().create();

    assertNotNull(service);
    assertTrue(service instanceof Service);
  }
}

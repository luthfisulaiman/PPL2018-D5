package id.meetu.meetu.util;

/**
 * Created by ricky on 11/03/18.
 */

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import id.meetu.meetu.BuildConfig;

import static junit.framework.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class ConstantTest {

    @Test
    public void testConstructModel() {
        Constant c = new Constant();
        assertNotNull(c);
    }
}

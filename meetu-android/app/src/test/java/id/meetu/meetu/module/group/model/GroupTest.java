package id.meetu.meetu.module.group.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.module.auth.model.UserProfile;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by ricky on 11/03/18.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class GroupTest {

    @Test
    public void testConstructModel() {
        Group group = new Group();
        assertNotNull(group);
    }

    @Test
    public void testSetterGetterId() {
        Group group = new Group();
        group.setId(1);
        assertEquals(group.getId(), 1);
    }

    @Test
    public void testSetterGetterTitle() {
        Group group = new Group();
        group.setTitle("Group Ricky");
        assertEquals(group.getTitle(), "Group Ricky");
    }

    @Test
    public void testSetterGetterOwner() {
        Group group = new Group();
        UserProfile userProfile = new UserProfile();
        group.setOwner(userProfile);
        assertNotNull(group.getOwner());
    }

}

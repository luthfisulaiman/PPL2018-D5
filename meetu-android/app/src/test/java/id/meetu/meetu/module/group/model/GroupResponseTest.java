package id.meetu.meetu.module.group.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 27/03/18.
 */
@RunWith(PowerMockRunner.class)
public class GroupResponseTest {

  @Test
  public void testConstructGroupResponse() {
    int owner = 1;
    String title = "meetu peeps";

    GroupResponse groupResponse = new GroupResponse(owner, title);

    assertEquals(owner, groupResponse.getOwner());
    assertEquals(title, groupResponse.getTitle());
  }

  @Test
  public void testSetterGetterOwner() {
    int owner = 1;
    String title = "meetu peeps";

    GroupResponse groupResponse = new GroupResponse(owner, title);

    groupResponse.setOwner(2);
    assertEquals(2, groupResponse.getOwner());
  }

  @Test
  public void testSetterGetterTitle() {
    int owner = 1;
    String title = "meetu peeps";

    GroupResponse groupResponse = new GroupResponse(owner, title);

    groupResponse.setTitle("risk tekers");
    assertEquals("risk tekers", groupResponse.getTitle());
  }
}

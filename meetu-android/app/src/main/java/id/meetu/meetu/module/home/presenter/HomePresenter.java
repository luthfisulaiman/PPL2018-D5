package id.meetu.meetu.module.home.presenter;

import id.meetu.meetu.module.meetup.model.MeetUpDetail;
import id.meetu.meetu.module.meetup.model.MeetUpResponse;
import id.meetu.meetu.module.meetup.service.MeetUpService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 27/03/18.
 */

public class HomePresenter {
  private HomePresenterListener listener;
  private MeetUpService meetUpService;

  public HomePresenter(HomePresenterListener listener) {
    this.listener = listener;
    this.meetUpService = new MeetUpService();
  }

  public HomePresenter(HomePresenterListener listener, MeetUpService meetUpService) {
    this.listener = listener;
    this.meetUpService = meetUpService;
  }

  public HomePresenterListener getListener() {
    return listener;
  }

  public MeetUpService getMeetUpService() {
    return meetUpService;
  }

  public void getMeetUps() {
    meetUpService.getMeetUps(new Callback<List<MeetUpResponse>>() {
      @Override
      public void onResponse(Call<List<MeetUpResponse>> call,
          Response<List<MeetUpResponse>> response) {
        if(response.isSuccessful()) {
          List<MeetUpDetail> meetUps = new ArrayList<>();
          int idx = 1;

          for(MeetUpResponse meetUp : response.body()) {
            String title = meetUp.getTitle();
            String group = meetUp.getGroupResponse().getTitle();
            Date startTime = meetUp.getStartTime();
            Date endTime = meetUp.getEndTime();

            String date = new SimpleDateFormat("EEEE, dd MMMM").format(startTime);
            String startDetail = new SimpleDateFormat("HH.mm").format(startTime);
            String endDetail = new SimpleDateFormat("HH.mm").format(endTime);
            String locationTime = startDetail + " - " + endDetail + " @ " + "MIC";

            MeetUpDetail detail = new MeetUpDetail(idx, title, group, date, locationTime);
            meetUps.add(detail);

            idx += 1;
          }
          listener.onGetSuccess(meetUps);
        } else {
          listener.onError("Something is wrong");
        }
      }

      @Override
      public void onFailure(Call<List<MeetUpResponse>> call, Throwable t) {
        listener.onError("Something is wrong with your connection");
      }
    });
  }
}

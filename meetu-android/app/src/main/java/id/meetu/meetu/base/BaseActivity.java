package id.meetu.meetu.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by stephen on 3/3/18.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @LayoutRes
    public abstract int findLayout();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(findLayout());
        ButterKnife.bind(this);
    }
}
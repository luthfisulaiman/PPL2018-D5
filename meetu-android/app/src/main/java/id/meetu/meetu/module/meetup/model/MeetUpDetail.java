package id.meetu.meetu.module.meetup.model;

/**
 * Created by luthfi on 11/03/18.
 */

public class MeetUpDetail {
    private int id;
    private String title;
    private String groups;
    private String date;
    private String locationTime;

    private final int MAX_ID = Integer.MAX_VALUE;

    public MeetUpDetail(int id, String title, String groups, String date, String locationTime) {
        this.id = id;
        this.title = title;
        this.groups = groups;
        this.date = date;
        this.locationTime = locationTime;
    }

    public MeetUpDetail() {
        this.id = MAX_ID;
        this.title = "";
        this.groups = "";
        this.date = "";
        this.locationTime = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

   public String getDate() {
        return date;
   }

   public void setDate(String date) {
        this.date = date;
   }

   public String getLocationTime() {
        return locationTime;
   }

   public void setLocationTime(String locationTime) {
        this.locationTime = locationTime;
   }
}

package id.meetu.meetu.module.meetup.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import id.meetu.meetu.module.group.model.Group;

/**
 * Created by ricky on 05/03/18.
 */

public class MeetUp {

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    @SerializedName("group_id")
    private int groupId;

    @SerializedName("start_time")
    private Date startTime;

    @SerializedName("end_time")
    private Date endTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}

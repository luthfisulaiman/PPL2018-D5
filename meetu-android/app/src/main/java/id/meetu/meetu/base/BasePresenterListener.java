package id.meetu.meetu.base;

/**
 * Created by ayaz97 on 04/03/18.
 */

public interface BasePresenterListener {
    void onError(String error);
}

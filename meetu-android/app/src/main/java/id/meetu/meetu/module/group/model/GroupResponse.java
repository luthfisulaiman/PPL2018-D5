package id.meetu.meetu.module.group.model;

import com.google.gson.annotations.SerializedName;
import id.meetu.meetu.base.BaseResponse;

/**
 * Created by ayaz97 on 27/03/18.
 */

public class GroupResponse extends BaseResponse {
  @SerializedName("owner")
  private int owner;

  @SerializedName("title")
  private String title;

  public GroupResponse(int owner, String title) {
    this.owner = owner;
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getOwner() {
    return owner;
  }

  public void setOwner(int owner) {
    this.owner = owner;
  }
}

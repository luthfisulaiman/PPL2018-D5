package id.meetu.meetu.module.auth.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.meetu.meetu.module.group.model.Group;

/**
 * Created by ricky on 05/03/18.
 */

public class User {

    @SerializedName("id")
    private int id;

    @SerializedName("username")
    private String username;

    @SerializedName("user_profile")
    private UserProfile userProfile;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}

package id.meetu.meetu.module.meetup.model;

import com.google.gson.annotations.SerializedName;
import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.group.model.GroupResponse;
import java.util.Date;

/**
 * Created by ayaz97 on 27/03/18.
 */

public class MeetUpResponse extends BaseResponse {
  @SerializedName("title")
  private String title;

  @SerializedName("group")
  private GroupResponse groupResponse;

  @SerializedName("start_time")
  private Date startTime;

  @SerializedName("end_time")
  private Date endTime;

  public MeetUpResponse(String title, GroupResponse groupResponse, Date startTime, Date endTime) {
    this.title = title;
    this.groupResponse = groupResponse;
    this.startTime = startTime;
    this.endTime = endTime;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public GroupResponse getGroupResponse() {
    return groupResponse;
  }

  public void setGroupResponse(GroupResponse groupResponse) {
    this.groupResponse = groupResponse;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }
}

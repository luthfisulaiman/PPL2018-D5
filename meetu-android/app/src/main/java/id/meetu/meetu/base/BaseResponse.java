package id.meetu.meetu.base;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ayaz97 on 04/03/18.
 */

public class BaseResponse {
    @SerializedName("success")
    private boolean success;

    public boolean getSuccess() {
        return success;
    }
}

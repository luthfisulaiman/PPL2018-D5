package id.meetu.meetu.base;

/**
 * Created by ayaz97 on 06/03/18.
 */

public class BaseException extends Exception {
  protected String message;

  public BaseException(String message) {
    super(message);
    this.message = message;
  }

  public String getMessage() {
    return message;
  }
}

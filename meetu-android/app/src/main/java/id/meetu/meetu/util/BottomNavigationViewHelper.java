package id.meetu.meetu.util;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.Menu;
import android.view.MenuItem;

import id.meetu.meetu.R;
import id.meetu.meetu.module.home.view.activity.HomeActivity;
import id.meetu.meetu.module.notification.view.NotificationActivity;
import id.meetu.meetu.module.profile.view.ProfileActivity;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

/**
 * Created by luthfi on 04/03/18.
 */

public class BottomNavigationViewHelper {

    public static void setupBottomNavigationView(final Context context, BottomNavigationViewEx bottomNav, final int ACTIVITY_NUM) {
        bottomNav.enableAnimation(false);
        bottomNav.enableItemShiftingMode(false);
        bottomNav.enableShiftingMode(false);
        bottomNav.setTextVisibility(false);
        enableNavigation(context, bottomNav);
        Menu menu = bottomNav.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);
    }

    public static void enableNavigation(final Context context, BottomNavigationViewEx view) {
        view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.setChecked(true);
                switch (item.getItemId()) {
                    case R.id.ic_profile:
                        Intent intent3 = new Intent(context, ProfileActivity.class); // ACTIVITY_NUM = 0
                        context.startActivity(intent3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                        break;

                    case R.id.ic_home:
                        Intent intent1 = new Intent(context, HomeActivity.class); // ACTIVITY_NUM = 1
                        context.startActivity(intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                        break;

                    case R.id.ic_notification:
                        Intent intent2 = new Intent(context, NotificationActivity.class); // ACTIVITY_NUM = 2
                        context.startActivity(intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                        break;
                }
                return false;
            }
        });
    }
}

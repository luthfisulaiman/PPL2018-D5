package id.meetu.meetu.module.auth.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.meetu.meetu.module.group.model.Group;

/**
 * Created by ricky on 11/03/18.
 */

public class UserProfile {

    @SerializedName("user_id")
    private int userId;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("location")
    private String location;

    @SerializedName("groups")
    private List<Group> groups;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
}

package id.meetu.meetu.module.meetup.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.meetu.meetu.module.meetup.model.MeetUpDetail;
import java.util.List;

import id.meetu.meetu.R;

/**
 * Created by luthfi on 11/03/18.
 */

public class MeetUpDetailAdapter extends RecyclerView.Adapter<MeetUpDetailAdapter.MeetUpViewHolder> {

    private Context mCtx;
    private List<MeetUpDetail> meetUpLists;

    public MeetUpDetailAdapter(Context mCtx, List<MeetUpDetail> meetUpLists) {
        this.mCtx = mCtx;
        this.meetUpLists = meetUpLists;
    }

    @Override
    public MeetUpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.layout_cardview, null);
        return new MeetUpViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MeetUpViewHolder holder, int position) {
        MeetUpDetail detail = meetUpLists.get(position);

        holder.textViewTitle.setText(detail.getTitle());
        holder.textViewGroup.setText(detail.getGroups());
        holder.textViewDate.setText(detail.getDate());
        holder.textViewLocationTime.setText(detail.getLocationTime());
    }

    @Override
    public int getItemCount() {
        return meetUpLists.size();
    }

    public void setMeetUpLists(List<MeetUpDetail> meetUpLists) {
        this.meetUpLists = meetUpLists;
        notifyDataSetChanged();
    }

    class MeetUpViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewTitle)
        TextView textViewTitle;

        @BindView(R.id.textViewGroup)
        TextView textViewGroup;

        @BindView(R.id.textViewDate)
        TextView textViewDate;

        @BindView(R.id.textViewLocationTime)
        TextView textViewLocationTime;

        public MeetUpViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
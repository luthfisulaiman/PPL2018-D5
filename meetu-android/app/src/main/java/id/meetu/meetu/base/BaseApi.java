package id.meetu.meetu.base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import id.meetu.meetu.util.Constant;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ayaz97 on 04/03/18.
 */

public abstract class BaseApi<E> {
    public abstract String findUrl();

    public abstract E create();

    protected Gson gsonBuild() {
        return new GsonBuilder()
                .setDateFormat(Constant.DATE_FORMAT)
                .create();
    }

    protected Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl(findUrl())
                .addConverterFactory(GsonConverterFactory.create(gsonBuild()))
                .build();
    }
}

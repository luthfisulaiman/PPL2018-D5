package id.meetu.meetu.module.notification.view;

import android.content.Context;
import android.os.Bundle;
import butterknife.BindView;
import butterknife.ButterKnife;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.util.BottomNavigationViewHelper;

/**
 * Created by luthfi on 04/03/18.
 */

public class NotificationActivity extends BaseActivity {

    private Context context = NotificationActivity.this;
    private static final int ACTIVITY_NUM = 2;
    @BindView(R.id.navbar_bottom) BottomNavigationViewEx bottomNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        BottomNavigationViewHelper.setupBottomNavigationView(NotificationActivity.this, bottomNav, ACTIVITY_NUM);
    }

    @Override
    public int findLayout() {
        return R.layout.activity_home;
    }
}

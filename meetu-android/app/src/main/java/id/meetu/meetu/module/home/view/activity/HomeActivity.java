package id.meetu.meetu.module.home.view.activity;

import android.os.Bundle;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import butterknife.BindView;
import butterknife.ButterKnife;

import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.module.home.view.activity.fragment.HomeFragment;
import id.meetu.meetu.util.BottomNavigationViewHelper;

public class HomeActivity extends BaseActivity {
    @BindView(R.id.navbar_bottom) BottomNavigationViewEx bottomNav;

    private static final int ACTIVITY_NUM = 1;

    @Override
    public int findLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        showHome();
        BottomNavigationViewHelper.setupBottomNavigationView(HomeActivity.this, bottomNav, ACTIVITY_NUM);
    }

    private void showHome() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_home, new HomeFragment()).commit();
    }
}

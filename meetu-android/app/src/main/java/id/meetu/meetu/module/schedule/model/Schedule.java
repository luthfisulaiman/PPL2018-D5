package id.meetu.meetu.module.schedule.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ricky on 05/03/18.
 */

public class Schedule {

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    @SerializedName("parent_id")
    private String parentId;

    @SerializedName("start_time")
    private Date startTime;

    @SerializedName("end_time")
    private Date endTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}

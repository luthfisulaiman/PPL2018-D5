package id.meetu.meetu.module.profile.view;

import android.content.Context;
import android.os.Bundle;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.util.BottomNavigationViewHelper;

/**
 * Created by luthfi on 04/03/18.
 */

public class ProfileActivity extends BaseActivity {

    private Context context = ProfileActivity.this;
    private static final int ACTIVITY_NUM = 0;

    @BindView(R.id.navbar_bottom) BottomNavigationViewEx bottomNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        BottomNavigationViewHelper.setupBottomNavigationView(ProfileActivity.this, bottomNav, ACTIVITY_NUM);
    }

    @Override
    public int findLayout() {
        return R.layout.activity_home;
    }
}

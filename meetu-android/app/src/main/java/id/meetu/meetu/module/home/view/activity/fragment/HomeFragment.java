package id.meetu.meetu.module.home.view.activity.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.meetu.meetu.R;
import id.meetu.meetu.module.home.presenter.HomePresenter;
import id.meetu.meetu.module.home.presenter.HomePresenterListener;
import id.meetu.meetu.module.meetup.model.MeetUpDetail;
import id.meetu.meetu.module.meetup.view.adapter.MeetUpDetailAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luthfi on 04/03/18.
 */

public class HomeFragment extends Fragment implements HomePresenterListener {

  @BindView(R.id.recyclerView)
  RecyclerView recyclerView;

  @BindView(R.id.fab)
  FloatingActionButton fab;

  private MeetUpDetailAdapter adapter;
  private HomePresenter presenter = new HomePresenter(this);

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_home, container, false);

    ButterKnife.bind(this, view);
    recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
    recyclerView.setHasFixedSize(true);

    List<MeetUpDetail> meetUpDetails = new ArrayList<>();
    adapter = new MeetUpDetailAdapter(view.getContext(), meetUpDetails);
    recyclerView.setAdapter(adapter);

    presenter.getMeetUps();

    return view;
  }

  public void onGetSuccess(List<MeetUpDetail> meetUpList) {
    adapter.setMeetUpLists(meetUpList);
  }

  public void onError(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
  }
}
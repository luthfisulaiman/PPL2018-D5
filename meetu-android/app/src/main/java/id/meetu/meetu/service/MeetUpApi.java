package id.meetu.meetu.service;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.base.BaseApi;
import id.meetu.meetu.module.meetup.model.MeetUpResponse;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by ayaz97 on 27/03/18.
 */

public class MeetUpApi extends BaseApi<MeetUpApi.Service> {

  @Override
  public String findUrl() {
    return BuildConfig.BACKEND_URL;
  }

  @Override
  public Service create() {
    return build().create(Service.class);
  }

  public interface Service {

    @GET("api/meetups/")
    Call<List<MeetUpResponse>> getMeetUps();
  }
}
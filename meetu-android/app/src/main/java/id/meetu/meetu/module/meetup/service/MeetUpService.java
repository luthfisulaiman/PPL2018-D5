package id.meetu.meetu.module.meetup.service;

import id.meetu.meetu.module.meetup.model.MeetUpResponse;
import id.meetu.meetu.service.MeetUpApi;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by ayaz97 on 27/03/18.
 */

public class MeetUpService {

  private MeetUpApi.Service service;

  public MeetUpService() {
    service = new MeetUpApi().create();
  }

  public MeetUpService(MeetUpApi.Service service) {
    this.service = service;
  }

  public MeetUpApi.Service getService() {
    return service;
  }

  public void getMeetUps(Callback<List<MeetUpResponse>> callback) {
    Call<List<MeetUpResponse>> call = service.getMeetUps();
    call.enqueue(callback);
  }
}

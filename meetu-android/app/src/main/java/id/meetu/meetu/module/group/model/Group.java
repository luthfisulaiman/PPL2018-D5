package id.meetu.meetu.module.group.model;

import com.google.gson.annotations.SerializedName;

import id.meetu.meetu.module.auth.model.UserProfile;

/**
 * Created by ricky on 05/03/18.
 */

public class Group {

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    @SerializedName("owner")
    private UserProfile owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UserProfile getOwner() {
        return owner;
    }

    public void setOwner(UserProfile owner) {
        this.owner = owner;
    }
}

package id.meetu.meetu.module.home.presenter;

import id.meetu.meetu.base.BasePresenterListener;
import id.meetu.meetu.module.meetup.model.MeetUpDetail;
import java.util.List;

/**
 * Created by ayaz97 on 27/03/18.
 */

public interface HomePresenterListener extends BasePresenterListener {

  void onGetSuccess(List<MeetUpDetail> meetUps);
}

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from meetu_backend.api.serializers.meetup import MeetUpSerializer
from meetu_backend.api.utils.meetup_seed_util import setup_seeding_data
from meetu_backend.apps.meetup.models import MeetUp

class MeetUpListView(APIView):

    def get(self, request, format=None):
        # TODO: Fix this later
        meetups = MeetUp.objects.filter()
        if meetups.count() == 0:
            setup_seeding_data()
        return Response(MeetUpSerializer(MeetUp.objects.filter().order_by('start_time'), many=True).data)

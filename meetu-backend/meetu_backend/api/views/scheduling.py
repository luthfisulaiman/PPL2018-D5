from datetime import datetime

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from meetu_backend.apps.scheduling.scheduling import BasicScheduling

class SchedulingListView(APIView):

    def post(self, request, format=None):
        data = request.data

        try:
            start_time = data.get('start_time', None)
            end_time = data.get('end_time', None)

            start_time = datetime.strptime(start_time, '%Y-%m-%dT%H:%M:%SZ')
            end_time = datetime.strptime(end_time, '%Y-%m-%dT%H:%M:%SZ')
        except Exception as e:
            response = {'error_message': 'Invalid date format'}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        repeat_type = data.get('repeat_type', None)
        duration = data.get('duration', None)

        basic_scheduling = BasicScheduling()

        try:
            availables = basic_scheduling.get_available_schedules(start_time, end_time, repeat_type, duration)
        except Exception as e:
            response = {'error_message': str(e)}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        
        return Response(availables, status=status.HTTP_200_OK)

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from meetu_backend.api.serializers.schedule import ScheduleSerializer
from meetu_backend.apps.schedule.models import Schedule

class ScheduleListView(APIView):

    def get(self, request, format=None):
        return Response(ScheduleSerializer(Schedule.objects.all(), many=True).data)

    def post(self, request, format=None):
        ser = ScheduleSerializer(data=request.data, many=True)
        if ser.is_valid():
            ser.save()
            return Response(ser.data)
        return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, format=None):
        parent_id = request.data.get('parent_id', None)
        Schedule.objects.filter(parent_id=parent_id).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

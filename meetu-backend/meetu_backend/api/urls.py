from django.conf.urls import url
from meetu_backend.api.views import (
    schedule,
    meetup,
    scheduling
)

urlpatterns = [
    # url(r'^$', root.api_root, name='api-root'),

    # ME
    url(r'^schedules/$', schedule.ScheduleListView.as_view(), name='schedule-list'),
    url(r'^meetups/$', meetup.MeetUpListView.as_view(), name='meetup-list'),
    url(r'^recommendation/$', scheduling.SchedulingListView.as_view(), name='recommendation-list')
]

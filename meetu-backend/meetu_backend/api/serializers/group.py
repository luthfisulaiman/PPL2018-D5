import uuid

from rest_framework import serializers

from meetu_backend.apps.group.models import Group

class GroupMinSerializer(serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = ['owner', 'title']

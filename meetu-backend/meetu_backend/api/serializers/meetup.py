import uuid

from rest_framework import serializers

from meetu_backend.apps.meetup.models import MeetUp
from meetu_backend.apps.schedule.models import Schedule
from meetu_backend.api.serializers.group import GroupMinSerializer

class MeetUpSerializer(serializers.ModelSerializer):

    group = GroupMinSerializer()

    class Meta:
        model = MeetUp
        fields = '__all__'
        read_only_fields = ['parent_id',]

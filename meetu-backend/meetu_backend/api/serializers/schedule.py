import uuid

from rest_framework import serializers

from meetu_backend.apps.schedule.models import Schedule

class ScheduleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Schedule
        fields = '__all__'
        read_only_fields = ['parent_id',]

    def validate(self, attrs):
        for schedule in Schedule.objects.filter():
            if ((schedule.start_time < attrs['end_time'] < schedule.end_time) or (attrs['start_time'] < schedule.end_time < attrs['end_time']) \
                or (attrs['start_time'] == schedule.start_time and attrs['end_time'] == schedule.end_time)):
                raise serializers.ValidationError('Conflict exists')
        attrs['parent_id'] = uuid.uuid4()
        return attrs

from datetime import datetime
from dateutil.relativedelta import relativedelta
from pytz import UTC

from django.contrib.auth import get_user_model
from meetu_backend.apps.authentication.models import UserProfile
from meetu_backend.apps.group.models import Group
from meetu_backend.apps.meetup.models import MeetUp

User = get_user_model()

def setup_seeding_data():
    user = User.objects.create(username='rickybau', password='sangatbau')
    user_profile = UserProfile.objects.create(user=user, phone_number='+9999999999', location='kolong jembatan')
    group = Group.objects.create(owner=user_profile, title='tim 7')

    meetups = [
        {
            'title': 'rapat petani',
            'group': group,
            'start_time': datetime(2018, 2, 1, 14, 30, tzinfo=UTC),
            'end_time': datetime(2018, 2, 1, 15, 15, tzinfo=UTC),
        },
        {
            'title': 'minum baygon',
            'group': group,
            'start_time': datetime(2018, 1, 31, 5, 0, tzinfo=UTC),
            'end_time': datetime(2018, 1, 31, 6, 15, tzinfo=UTC)
        }
    ]

    for meetup in meetups:
        title = meetup['title']
        start_time = meetup['start_time']
        end_time = meetup['end_time']
        meetup_group = meetup['group']

        MeetUp.objects.create(group=meetup_group, title=title, start_time=start_time, end_time=end_time)

import json

from datetime import datetime    

from django.urls import reverse

from rest_framework.test import APITestCase
from rest_framework import status

from meetu_backend.apps.meetup.models import MeetUp

class MeetUpTestCase(APITestCase):

    def test_can_get_all_meetup(self):
        response = self.client.get(reverse('meetup-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

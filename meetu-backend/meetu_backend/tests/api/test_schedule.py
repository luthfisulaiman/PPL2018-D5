from datetime import datetime    

from django.urls import reverse

from rest_framework.test import APITestCase
from rest_framework import status

from meetu_backend.apps.schedule.models import Schedule

class ScheduleListViewTestCase(APITestCase):

    @classmethod
    def setUpTestData(cls):
        Schedule.objects.create(title='Test 1', start_time=datetime.now(), end_time=datetime.now())

    def test_can_post_schedules(self):
        data = [
            {
                'title': 'Test 2',
                'start_time': '2018-03-06T23:59',
                'end_time':'2018-03-06T23:59'
            }
        ]
        response = self.client.post(reverse('schedule-list'), data=data, format='json')
        self.assertEqual(Schedule.objects.filter(title='Test 2').count(), 1)

    def test_cannot_post_schedule_with_same_time_from_2_different_request(self):
        data = [
            {
                'title': 'Test 3',
                'start_time': '2018-03-06T23:59',
                'end_time':'2018-03-06T23:59'
            }
        ]
        response = self.client.post(reverse('schedule-list'), data=data, format='json')
        response = self.client.post(reverse('schedule-list'), data=data, format='json')
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_can_delete_schedule_by_parent_id(self):
        data = {
            'parent_id': Schedule.objects.filter(title='Test 1').first().parent_id
        }
        response = self.client.delete(reverse('schedule-list'), data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Schedule.objects.filter(title='Test 1').count(), 0)
    
    def test_can_get_all_schedule(self):
        response = self.client.get(reverse('schedule-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

from datetime import datetime
from pytz import UTC

from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status

from meetu_backend.tests.utils.utils import setup_scheduling_test

class SchedulingListViewTestCase(APITestCase):

    @classmethod
    def setUpTestData(cls):
        setup_scheduling_test()
    
    def test_get_invalid_request_time(self):
        data = {
            'start_time': 1,
            'end_time': 2,
        }

        response = self.client.post(reverse('recommendation-list'), data=data, format='json')
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
    
    def test_get_invalid_request_general(self):
        data = {
            'start_time': datetime(2018, 2, 1, tzinfo=UTC),
            'end_time': datetime(2018, 2, 1, tzinfo=UTC),
            'repeat_type': -1,
            'duration': 1
        }

        response = self.client.post(reverse('recommendation-list'), data=data, format='json')
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_get_recommendation(self):
        data = {
            'start_time': datetime(2018, 2, 1, tzinfo=UTC),
            'end_time': datetime(2018, 2, 1, tzinfo=UTC),
            'repeat_type': 'ONCE',
            'duration': 1
        }

        expected_data = {
            'available_times': [
                {
                    'end_time': datetime(2018, 2, 1, 7, 0), 
                    'start_time': datetime(2018, 2, 1, 0, 0)
                }, 
                {
                    'end_time': datetime(2018, 2, 1, 13, 0), 
                    'start_time': datetime(2018, 2, 1, 12, 45)
                }, 
                {
                    'end_time': datetime(2018, 2, 1, 14, 30), 
                    'start_time': datetime(2018, 2, 1, 14, 15)
                }, 
                {
                    'end_time': datetime(2018, 2, 2, 0, 0), 
                    'start_time': datetime(2018, 2, 1, 15, 15)
                }
            ]
        }
        
        response = self.client.post(reverse('recommendation-list'), data=data, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(expected_data, response.data)


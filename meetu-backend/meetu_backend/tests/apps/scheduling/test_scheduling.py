from django.test import TestCase
from datetime import datetime
from dateutil.relativedelta import relativedelta
from pytz import UTC

from meetu_backend.apps.schedule.models import Schedule
from meetu_backend.apps.meetup.models import MeetUp

from meetu_backend.apps.scheduling.scheduling import BasicScheduling

from meetu_backend.tests.utils.utils import setup_scheduling_test

class BasicSchedulingTest(TestCase):
    def setUp(self):
        setup_scheduling_test()
        self.basic_scheduling = BasicScheduling()

    def test_convert_to_minute(self):
        time = datetime(2018, 1, 31, 7, 7)
        self.assertEqual(427, BasicScheduling.convert_to_minute(time))

        time = datetime(2018, 1, 31, 0, 0)
        self.assertEqual(0, BasicScheduling.convert_to_minute(time))

        time = datetime(2018, 1, 31, 23, 59)
        self.assertEqual(1439, BasicScheduling.convert_to_minute(time))
    
    def test_get_next_date_daily(self):
        time = datetime(2018, 1, 30)
        self.assertEqual(datetime(2018, 1, 31), BasicScheduling.get_next_date(time, "DAILY", 1))
        self.assertEqual(datetime(2018, 2, 1), BasicScheduling.get_next_date(time, "DAILY", 2))

        time = datetime(2018, 12, 31)
        self.assertEqual(time, BasicScheduling.get_next_date(time, "DAILY", 0))
        self.assertEqual(datetime(2019, 1, 1), BasicScheduling.get_next_date(time, "DAILY", 1))
    
    def test_get_next_date_weekly(self):
        time = datetime(2018, 1, 30)
        self.assertEqual(datetime(2018, 2, 6), BasicScheduling.get_next_date(time, "WEEKLY", 1))
        self.assertEqual(datetime(2018, 2, 13), BasicScheduling.get_next_date(time, "WEEKLY", 2))

        time = datetime(2018, 12, 31)
        self.assertEqual(time, BasicScheduling.get_next_date(time, "WEEKLY", 0))
        self.assertEqual(datetime(2019, 1, 7), BasicScheduling.get_next_date(time, "WEEKLY", 1))
    
    def test_get_next_date_monthly(self):
        time = datetime(2018, 1, 30)
        self.assertEqual(datetime(2018, 2, 28), BasicScheduling.get_next_date(time, "MONTHLY", 1))
        self.assertEqual(datetime(2018, 3, 30), BasicScheduling.get_next_date(time, "MONTHLY", 2))

        time = datetime(2018, 12, 31)
        self.assertEqual(time, BasicScheduling.get_next_date(time, "MONTHLY", 0))
        self.assertEqual(datetime(2019, 1, 31), BasicScheduling.get_next_date(time, "MONTHLY", 1))

    def test_find_daily_available_schedule(self):
        start = datetime(2018, 2, 1, tzinfo=UTC)
        end = datetime(2018, 2, 2, tzinfo=UTC)

        schedules = list(Schedule.objects.filter(start_time__gte=start, end_time__lt=end))
        schedules.extend(list(MeetUp.objects.filter(start_time__gte=start, end_time__lt=end)))

        expected = set(range(60 * 24))
        for schedule in schedules:
            start = BasicScheduling.convert_to_minute(schedule.start_time)
            end = BasicScheduling.convert_to_minute(schedule.end_time)

            for i in range(start, end):
                expected.discard(i)
        
        result = self.basic_scheduling.find_daily_available_schedule(schedules, 0)
        self.assertEqual(expected, result)
    
    def test_get_raw_available_schedules(self):
        start = datetime(2018, 2, 5, tzinfo=UTC)
        end = datetime(2018, 2, 9, tzinfo=UTC)

        schedules = list(Schedule.objects.filter(start_time__gte=start, end_time__lt=end))
        schedules.extend(list(MeetUp.objects.filter(start_time__gte=start, end_time__lt=end)))

        expected = []
        for i in range(9 - 5):
            cur_day = start + relativedelta(days=i)
            cur_end = cur_day + relativedelta(days=1)
            available = set(range(60 * 24))

            for schedule in schedules:
                if (cur_day <= schedule.start_time) and (schedule.end_time < cur_end):
                    start_min = BasicScheduling.convert_to_minute(schedule.start_time)
                    end_min = BasicScheduling.convert_to_minute(schedule.end_time)

                    for minute in range(start_min, end_min):
                        available.discard(minute)

            expected.append(available) 

        result = self.basic_scheduling.get_raw_available_schedules(start, end, 0)

        self.assertEqual(expected, result)
    
    def test_get_schedules_minute_status_daily(self):
        start = datetime(2018, 1, 31)
        end = datetime(2018, 2, 8)

        raws = [set(range(60 * 24)) for i in range(8)]
        expected = [[True for j in range(60*24)] for i in range(8)]
        result = self.basic_scheduling.get_schedules_minute_status(raws, start, end, 'DAILY', 1)
        self.assertEqual(expected, result)

        raws = [set(range(60 * 24)) for i in range(8)]
        expected = [[True for j in range(60*24)] for i in range(6)]
        result = self.basic_scheduling.get_schedules_minute_status(raws, start, end, 'DAILY', 3)
        self.assertEqual(expected, result)

        raws[0].discard(0)
        raws[1].discard(77)
        expected[0][0] = False
        expected[0][77] = False
        expected[1][77] = False
        result = self.basic_scheduling.get_schedules_minute_status(raws, start, end, 'DAILY', 3)
        self.assertEqual(expected, result)

    def test_get_schedules_minute_status_weekly(self):
        start = datetime(2018, 1, 31)
        end = datetime(2018, 2, 8)

        raws = [set(range(60 * 24)) for i in range(8)]
        expected = [[True for j in range(60 * 24)] for i in range(1)]
        result = self.basic_scheduling.get_schedules_minute_status(raws, start, end, 'WEEKLY', 2)
        self.assertEqual(expected, result)

        raws[0].discard(100)
        raws[7].discard(80)
        expected[0][100] = False
        expected[0][80] = False
        result = self.basic_scheduling.get_schedules_minute_status(raws, start, end, 'WEEKLY', 2)
        self.assertEqual(expected, result)
    
    def test_get_schedules_minute_status_monthly(self):
        start = datetime(2018, 1, 31)
        end = datetime(2018, 3, 1)

        raws = [set(range(60 * 24)) for i in range(29)]
        expected = [[True for j in range(60 * 24)] for i in range(1)]
        result = self.basic_scheduling.get_schedules_minute_status(raws, start, end, 'MONTHLY', 2)
        self.assertEqual(expected, result)

        raws[0].discard(0)
        raws[28].discard(1)
        expected[0][0] = False
        expected[0][1] = False
        result = self.basic_scheduling.get_schedules_minute_status(raws, start, end, 'MONTHLY', 2)
        self.assertEqual(expected, result)
    
    def test_get_available_schedules_interval(self):
        start = datetime(2018, 1, 31)

        status = [[True for i in range(60 * 24)]]
        status.append([False for i in range(60 * 24)])

        expected = [
            {
                'start_time': datetime(2018, 1, 31, 0, 0),
                'end_time': datetime(2018, 2, 1, 0, 0)
            }
        ]
        result = self.basic_scheduling.get_available_schedules_interval(status, start)
        self.assertEqual(expected, result)

        status[0][23] = False
        status[0][24] = False
        status[0][26] = False
        status[1][5] = True
        expected = [
            {
                'start_time': datetime(2018, 1, 31, 0, 0),
                'end_time': datetime(2018, 1, 31, 0, 23)
            },
            {
                'start_time': datetime(2018, 1, 31, 0, 25),
                'end_time': datetime(2018, 1, 31, 0, 26)
            },
            {
                'start_time': datetime(2018, 1, 31, 0, 27),
                'end_time': datetime(2018, 2, 1, 0, 0)
            },
            {
                'start_time': datetime(2018, 2, 1, 0, 5),
                'end_time': datetime(2018, 2, 1, 0, 6)
            },
        ]
        result = self.basic_scheduling.get_available_schedules_interval(status, start)
        self.assertEqual(expected, result)

    def test_get_available_schedule_invalid_date(self):
        start = datetime(2018, 2, 1)
        end = datetime(2018, 1, 30)
        result = {}

        with self.assertRaises(ValueError) as e:
            result = self.basic_scheduling.get_available_schedules(start, end, None, None)
        self.assertEqual("Start date must be lower or equal than end date", str(e.exception))
    
    def test_get_available_schedule_invalid_repeat_type(self):
        start = datetime(2018, 1, 1)
        end = datetime(2018, 1, 30)
        result = {}

        with self.assertRaises(ValueError) as e:
            result = self.basic_scheduling.get_available_schedules(start, end, None, None)
        self.assertEqual("Repeat type must be either 'ONCE', 'DAILY', 'WEEKLY', 'MONTHLY'", str(e.exception))
    
    def test_get_available_schedule_invalid_duration(self):
        start = datetime(2018, 1, 1)
        end = datetime(2018, 1, 30)
        repeat_type = "DAILY"
        result = {}

        with self.assertRaises(ValueError) as e:
            result = self.basic_scheduling.get_available_schedules(start, end, repeat_type, -99)
        self.assertEqual("Duration must be higher than 0", str(e.exception))
    
    def test_get_available_schedule_invalid_threshold(self):
        start = datetime(2018, 1, 1)
        end = datetime(2018, 1, 30)
        repeat_type = "DAILY"
        duration = 1
        thres = -1
        result = {}

        with self.assertRaises(ValueError) as e:
            result = self.basic_scheduling.get_available_schedules(start, end, repeat_type, duration, thres=thres)
        self.assertEqual("Minimum unavailable person must be >= 0", str(e.exception))
    
    def test_get_available_schedule(self):
        start = datetime(2018, 2, 1)
        end = datetime(2018, 2, 1)
        repeat_type = "ONCE"
        duration = 1

        result = self.basic_scheduling.get_available_schedules(start, end, repeat_type, duration)
        expected = [
            {
                'start_time': datetime(2018, 2, 1, 0, 0), 
                'end_time': datetime(2018, 2, 1, 7, 0)
            }, 
            {
                'start_time': datetime(2018, 2, 1, 12, 45), 
                'end_time': datetime(2018, 2, 1, 13, 0)
            }, 
            {
                'start_time': datetime(2018, 2, 1, 14, 15), 
                'end_time': datetime(2018, 2, 1, 14, 30)
            }, 
            {
                'start_time': datetime(2018, 2, 1, 15, 15), 
                'end_time': datetime(2018, 2, 2, 0, 0)
            }
        ]
        expected = {'available_times': expected}

        self.assertEqual(expected, result)
from datetime import datetime
from dateutil.relativedelta import relativedelta
from pytz import UTC

from django.contrib.auth import get_user_model
from meetu_backend.apps.authentication.models import UserProfile
from meetu_backend.apps.group.models import Group
from meetu_backend.apps.schedule.models import Schedule
from meetu_backend.apps.meetup.models import MeetUp

from meetu_backend.apps.scheduling.scheduling import BasicScheduling

User = get_user_model()

def setup_scheduling_test():
    user = User.objects.create(username='rickybau', password='sangatbau')
    user_profile = UserProfile.objects.create(user=user, phone_number='+9999999999', location='kolong jembatan')
    group = Group.objects.create(owner=user_profile, title='tim 7')

    schedules = [
        {
            'title': 'kuliah1',
            'start_time': datetime(2018, 2, 1, 7, 0, tzinfo=UTC),
            'end_time': datetime(2018, 2, 1, 11, 45, tzinfo=UTC),
            'repeat': 'DAILY',
            'duration': 5
        },
        {
            'title': 'kuliah2',
            'start_time': datetime(2018, 2, 1, 13, 0, tzinfo=UTC),
            'end_time': datetime(2018, 2, 1, 14, 15, tzinfo=UTC),
            'repeat': 'DAILY',
            'duration': 5
        },
        {
            'title': 'panen meme',
            'start_time': datetime(2018, 2, 1, 12, 15, tzinfo=UTC),
            'end_time': datetime(2018, 2, 1, 12, 45, tzinfo=UTC),
            'repeat': 'WEEKLY',
            'duration': 5
        },
        {
            'title': 'baca 9gag',
            'start_time': datetime(2018, 2, 1, 10, 0, tzinfo=UTC),
            'end_time': datetime(2018, 2, 1, 12, 30, tzinfo=UTC),
            'repeat': 'DAILY',
            'duration': 5
        }
    ]

    meetups = [
        {
            'title': 'rapat petani',
            'group': group,
            'start_time': datetime(2018, 2, 1, 14, 30, tzinfo=UTC),
            'end_time': datetime(2018, 2, 1, 15, 15, tzinfo=UTC),
            'repeat': 'MONTHLY',
            'duration': 2
        }
    ]

    for schedule in schedules:
        title = schedule['title']
        repeat = schedule['repeat']
        duration = schedule['duration']
        start_time = schedule['start_time']
        end_time = schedule['end_time']

        for i in range(duration):
            cur_start = BasicScheduling.get_next_date(start_time, repeat, i)
            cur_end = BasicScheduling.get_next_date(end_time, repeat, i)
            Schedule.objects.create(start_time=cur_start, end_time=cur_end, title=title)
    
    for meetup in meetups:
        title = meetup['title']
        repeat = meetup['repeat']
        duration = meetup['duration']
        start_time = meetup['start_time']
        end_time = meetup['end_time']
        meetup_group = meetup['group']

        for i in range(duration):
            cur_start = BasicScheduling.get_next_date(start_time, repeat, i)
            cur_end = BasicScheduling.get_next_date(end_time, repeat, i)
            MeetUp.objects.create(group=meetup_group, title=title, start_time=cur_start, end_time=cur_end)
    
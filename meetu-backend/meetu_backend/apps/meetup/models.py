from django.db import models

from meetu_backend.apps.group.models import Group

# Create your models here.
class MeetUp(models.Model):
    title = models.CharField(max_length=128)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

from datetime import datetime
from dateutil.relativedelta import relativedelta
from pytz import UTC

from meetu_backend.apps.schedule.models import Schedule
from meetu_backend.apps.meetup.models import MeetUp

class BasicScheduling:
    @staticmethod
    def convert_to_minute(time):
        return 60 * time.hour + time.minute
    
    @staticmethod
    def get_next_date(date, repeat_type, delta):
        result = date
        if repeat_type == "DAILY":
            result = result + relativedelta(days=delta)
        if repeat_type == "WEEKLY":
            result = result + relativedelta(weeks=delta)
        if repeat_type == "MONTHLY":
            result = result + relativedelta(months=delta)
        return result
    
    def find_daily_available_schedule(self, schedules, thres):
        unavailable = [0 for i in range(60 * 24)]

        for schedule in schedules:
            start_time = BasicScheduling.convert_to_minute(schedule.start_time)
            end_time = BasicScheduling.convert_to_minute(schedule.end_time)

            unavailable[start_time] += 1
            unavailable[end_time] -= 1
        
        for i in range(1, 60 * 24):
            unavailable[i] += unavailable[i-1]
        
        available = set()
        for i in range(60 * 24):
            if unavailable[i] <= thres:
                available.add(i)
        
        return available
    
    def get_raw_available_schedules(self, start_date, end_date, thres):
        simple_start = datetime(start_date.year, start_date.month, start_date.day, tzinfo=UTC)
        simple_end = datetime(end_date.year, end_date.month, end_date.day, tzinfo=UTC)

        # TODO for next sprint: extract object from group
        schedules = list(Schedule.objects.filter(start_time__gte=simple_start, end_time__lt=simple_end))
        schedules.extend(list(MeetUp.objects.filter(start_time__gte=simple_start, end_time__lt=simple_end)))

        schedules.sort(key=lambda schedule: schedule.start_time)
        iterator = 0
        availables = []

        current_date = start_date
        while current_date < end_date:
            year = current_date.year
            month = current_date.month
            day = current_date.day

            simple_current_date = datetime(year, month, day)

            current_date_schedules = []
            while (iterator < len(schedules)):
                iterator_date = schedules[iterator].start_time
                simple_iterator_date = datetime(iterator_date.year, iterator_date.month, iterator_date.day)

                if simple_iterator_date == simple_current_date:
                    current_date_schedules.append(schedules[iterator])
                    iterator += 1
                else:
                    break
                
            current_availables = self.find_daily_available_schedule(current_date_schedules, thres)
            availables.append(current_availables)
            
            current_date = current_date + relativedelta(days=1)
            
        return availables
    
    def get_schedules_minute_status(self, availables, start_date, end_date, repeat_type, duration):
        minutes_status = []

        for i in range(len(availables)):
            day = start_date + relativedelta(days=i)

            if BasicScheduling.get_next_date(day, repeat_type, duration-1) < end_date:
                possible_minutes = [True for j in range(60 * 24)]
                
                for rel_date in range(duration):
                    cur_date = BasicScheduling.get_next_date(day, repeat_type, rel_date)
                    index = (cur_date - start_date).days

                    for minute in range(60 * 24):
                        if minute not in availables[index]:
                            possible_minutes[minute] = False
                
                minutes_status.append(possible_minutes)
        
        return minutes_status
    
    def get_available_schedules_interval(self, minutes_status, start_date):
        intervals = []

        for i in range(len(minutes_status)):
            status = minutes_status[i]
            day = start_date + relativedelta(days=i)
            minute = 0

            while minute < 60 * 24:
                if status[minute]:
                    end_min = minute
                    while (end_min < 60 * 24) and (status[end_min]):
                        end_min += 1

                    start_time = datetime(day.year, day.month, day.day, minute // 60, minute % 60)
                    if (end_min == 60 * 24):
                        end_time = datetime(day.year, day.month, day.day, 0, 0)
                        end_time += relativedelta(days=1)
                    else:
                        end_time = datetime(day.year, day.month, day.day, end_min // 60, end_min % 60)

                    interval = {}
                    interval['start_time'] = start_time
                    interval['end_time'] = end_time
                    intervals.append(interval)

                    minute = end_min
                else:
                    minute += 1
        
        return intervals
    
    def get_available_schedules(self, start_date, end_date, repeat_type, duration, thres=0):
        if start_date > end_date:
            raise ValueError("Start date must be lower or equal than end date")
        elif (repeat_type != "ONCE") and (repeat_type != "DAILY") and (repeat_type != "WEEKLY") and (repeat_type != "MONTHLY"):
            raise ValueError("Repeat type must be either 'ONCE', 'DAILY', 'WEEKLY', 'MONTHLY'")
        elif duration <= 0:
            raise ValueError("Duration must be higher than 0")
        elif thres < 0:
            raise ValueError("Minimum unavailable person must be >= 0")

        # make exclusive right
        end_date = end_date + relativedelta(days=1)

        # equivalent; better than treating it differently
        if repeat_type == "ONCE":
            repeat_type = "DAILY"
            duration = 1

        availables = self.get_raw_available_schedules(start_date, end_date, thres)
        minutes_status = self.get_schedules_minute_status(availables, start_date, end_date, repeat_type, duration)
        available_schedules = self.get_available_schedules_interval(minutes_status, start_date)

        available_schedules = {
            'available_times': available_schedules
        }

        return available_schedules

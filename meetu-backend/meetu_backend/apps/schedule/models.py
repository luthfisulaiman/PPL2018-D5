import uuid

from django.db import models

# Create your models here.
class Schedule(models.Model):
    title = models.CharField(max_length=128)
    parent_id = models.UUIDField(default=uuid.uuid4)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

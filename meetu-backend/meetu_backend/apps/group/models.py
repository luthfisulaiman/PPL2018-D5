from django.db import models

from meetu_backend.apps.authentication.models import UserProfile

# Create your models here.
class Group(models.Model):
    owner = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    title = models.CharField(max_length=256)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

class Membership(models.Model):
    MEMBERSHIP_TYPE = (
        ('OWNER', 'Owner'),
        ('MEMBER', 'Member'),
    )
    user_profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    type = models.CharField(max_length=8, choices=MEMBERSHIP_TYPE)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)


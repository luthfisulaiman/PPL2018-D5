# MeetU Android & Backend Service

MeetU is an online platform which intended to help people held a meetup based on their weekly activities and their tight schedule. MeetU will algorithmically recommend a meetup schedule based on their free schedules and priorities that are given. These priorities may vary across activities such as school work may be given higher priority than playing type activities.

## master
- Pipeline: [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/badges/master/pipeline.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/commits/master)
- Coverage: [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/badges/master/coverage.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/commits/master)
- Android Coverage: [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/badges/master/coverage.svg?job=unit_test_android_release)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/commits/master)
- Backend Coverage [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/badges/master/coverage.svg?job=unit_test_backend_release)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/commits/master)

## sit_uat
- Pipeline: [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/badges/sit_uat/pipeline.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/commits/sit_uat)
- Coverage: [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/badges/sit_uat/coverage.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/commits/sit_uat)
- Android Coverage: [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/badges/sit_uat/coverage.svg?job=unit_test_android_debug)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/commits/sit_uat)
- Backend Coverage [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/badges/sit_uat/coverage.svg?job=unit_test_backend_debug)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/commits/sit_uat)


## Getting Started

### Prerequisites

- Android SDK v17

Clone the repository

```bash
git clone https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5.git [Folder_Name]
```

### MeetU - Android

This project uses the Gradle build system for Android development. To build this project, simply click build project in Android Studio, or :

```bash
gradle build
gradle assembleRelease
```

### MeetU - Backend

This project uses the Django Framework for Backend and API Calls. To run locally :

```python
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```

## Running the tests

### Android unit tests

Create unit test under `/app/src/test/java/id/meetu/meetu/`. To run it :

```bash
./gradlew jacocoTestDebugReport
```

Report will be generated under `app/build/reports/jacoco/jacocoTestDebugReport/html/index.html`

#### Coding style tests

This tests will check the coding styles for all files, also known as 'linter' test :

```bash
./gradlew lint
```

### Python unit tests

Create unit test under `/meetu_backend/tests`. To run it, move to `meetu-backend` directory. Then run :

```bash
coverage run manage.py test meetu-backend.tests
coverage report
```

## Built With

* [ButterKnife](https://github.com/JakeWharton/butterknife) - Bind Android Views and callbacks to fields and methods
* [Retrofit2](http://square.github.io/retrofit/) - HTTP Requests for Android and Java
* [JUnit](https://github.com/junit-team/junit4) - Testing framework for Java
* [Mockito](https://github.com/mockito/mockito) - Mocking framework for unit tests
* [Django REST Framework](http://www.django-rest-framework.org/) - REST Framework used
* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D5/tags).

## Authors

[MeetU Engineering](https://medium.com/meetu-engineering)
